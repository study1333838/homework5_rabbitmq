package com.springboot.rabbitmq.demo;



import com.springboot.rabbitmq.demo.config.RabbitmqDirectConfig;
import com.springboot.rabbitmq.demo.config.RabbitmqFanoutConfig;
import com.springboot.rabbitmq.demo.config.RabbitmqTopicConfig;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class DemoApplicationTests {

    @Autowired
    private RabbitTemplate rabbitTemplate;

    @Test
    public void DirectExchange() {
        rabbitTemplate.convertAndSend(RabbitmqDirectConfig.EXCHANGE_NAME, RabbitmqDirectConfig.ROUTING_KEY, "boot mq hello Direct");
        System.out.println("发送消息boot mq hello Direct成功");
    }
    //这个我测试没成功！
    @Test
    public void DirectDefaultExchange(){
       rabbitTemplate.convertAndSend(RabbitmqDirectConfig.QUEUE_NAME, "boot mq hello Direct default");
        System.out.println("发送消息boot mq hello Direct  default成功");

    }


    @Test
    public void TopicExchange() {
        rabbitTemplate.convertAndSend(RabbitmqTopicConfig.EXCHANGE_NAME,"boot.haha","boot mq hello Topic");
        System.out.println("发送消息boot mq hello Topic 成功");
    }

    @Test
    public void FanoutExchange(){
        for(int i=0;i<4;i++){
            rabbitTemplate.convertAndSend(RabbitmqFanoutConfig.EXCHANGE_NAME,"",i+"===boot mq hello Fanout");
        }
        System.out.println("发送消息boot mq hello Fanout 成功");
    }

}

