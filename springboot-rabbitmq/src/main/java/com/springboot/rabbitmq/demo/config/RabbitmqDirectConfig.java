package com.springboot.rabbitmq.demo.config;

import org.springframework.amqp.core.*;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class RabbitmqDirectConfig {

    public static final String QUEUE_NAME = "QUEUE_DEMO_DIRECT";

    public static final String EXCHANGE_NAME = "direct_exchange";

    public static final String ROUTING_KEY = "ROUTING_KEY_01";

    @Bean("bootDirectExchange")
    public Exchange bootDirectExchange(){
        return ExchangeBuilder.directExchange(EXCHANGE_NAME).durable(true).build();
    }

    @Bean("bootDirectQueue")
    public Queue bootDirectQueue(){
        return QueueBuilder.durable(QUEUE_NAME).build();
    }

    @Bean
    public Binding bindDirectQueueExchange(@Qualifier("bootDirectQueue") Queue queue, @Qualifier("bootDirectExchange") Exchange exchange){
        return BindingBuilder.bind(queue).to(exchange).with(ROUTING_KEY).noargs();
    }

}
