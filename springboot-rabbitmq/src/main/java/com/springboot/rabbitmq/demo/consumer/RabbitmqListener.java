package com.springboot.rabbitmq.demo.consumer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;


@Component
public class RabbitmqListener {

  private Logger logger= LoggerFactory.getLogger(RabbitmqListener.class);


    @RabbitListener(queues = "QUEUE_DEMO_DIRECT")
    public void ListenerQueue01(Message message){
        System.out.println("mess===="+message);
        logger.info("[onMessage][线程编号:{} 消息内容：{}]", Thread.currentThread().getId(), message);
    }

    @RabbitListener(queues = "QUEUE_DEMO_TOPIC")
    public void ListenerQueue2(Message message){
        System.out.println("mess===="+message);
        logger.info("[onMessage][线程编号:{} 消息内容：{}]", Thread.currentThread().getId(), message);
    }

    // fanout 模式的
    @RabbitListener(queues = "QUEUE_DEMO_FANOUT_A")
    public void ListenerQueueA(Message message){
        System.out.println("QUEUE_DEMO_FANOUT_A======="+message);
        logger.info("[onMessage][线程编号:{} 消息内容：{}]", Thread.currentThread().getId(), message);
    }
    @RabbitListener(queues = "QUEUE_DEMO_FANOUT_B")
    public void ListenerQueueB(Message message){
        System.out.println("QUEUE_DEMO_FANOUT_B======="+message);
        logger.info("[onMessage][线程编号:{} 消息内容：{}]", Thread.currentThread().getId(), message);
    }
}
