package com.springboot.rabbitmq.demo.config;


import org.springframework.amqp.core.*;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class RabbitmqFanoutConfig {


    private static final String QUEUE_NAME_A="QUEUE_DEMO_FANOUT_A";
    private static final String QUEUE_NAME_B="QUEUE_DEMO_FANOUT_B";
    public static final String EXCHANGE_NAME="fanout_exchange";

    @Bean("bootFanoutExchange")
    public Exchange bootFanoutExchange(){
        return ExchangeBuilder.fanoutExchange(EXCHANGE_NAME).durable(true).build();
    }

    @Bean("bootFanoutQueueA")
    public Queue bootFanoutQueueA(){
        return QueueBuilder.durable(QUEUE_NAME_A).build();
    }

    @Bean("bootFanoutQueueB")
    public Queue bootFanoutQueueB(){
        return QueueBuilder.durable(QUEUE_NAME_B).build();
    }


    @Bean
    public Binding bindFanoutQueueExchangeA(@Qualifier("bootFanoutQueueA") Queue queue,@Qualifier("bootFanoutExchange") Exchange exchange){
        return BindingBuilder.bind(queue).to(exchange).with("").noargs();
    }

    @Bean
    public Binding bindFanoutQueueExchangeB(@Qualifier("bootFanoutQueueB") Queue queue,@Qualifier("bootFanoutExchange") Exchange exchange){
        return BindingBuilder.bind(queue).to(exchange).with("").noargs();
    }

}
